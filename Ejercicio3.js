var letterList = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var idNumber = window.prompt("Introduce tu número de DNI:");
var idLetter = window.prompt("Introduce la letra de tu DNI");

/** Esta función recoge el número introducido por el usuario y lo divide entre 23, obteniendo el resto, 
 * que es el número que se emplea para localizar la letra que corresponde al DNI y devuelve esta letra*/
function checkLetter (num) {
    var rest = num % 23;
    console.log(rest);
    var letter = letterList[rest];
    console.log(letter);
    return letter;
}

if (idNumber < 0 || idNumber > 99999999) {
    window.alert("El número proporcionado no es válido");
}
else { 
    
    var correctLetter = checkLetter(idNumber);

    if (correctLetter.toLowerCase === idLetter.toLowerCase){
            window.alert("DNI válido");
        }
    else {
            window.alert("DNI incorrecto");
        }
}